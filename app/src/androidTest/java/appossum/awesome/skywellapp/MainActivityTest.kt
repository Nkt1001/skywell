package appossum.awesome.skywellapp

import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.FlakyTest
import android.support.test.rule.ActivityTestRule
import android.util.Log
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.content_main.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by nikitaemelyanov on 02.05.2018.
 */
class MainActivityTest {

    @get:Rule val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java, true, false)

//    @get:Rule val runtimePermissionRule = GrantPermissionRule.grant(Manifest.permission.WRITE_SECURE_SETTINGS)

    @Before
    fun setup() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getInstrumentation().uiAutomation.executeShellCommand(
//                    "pm grant " + getTargetContext().packageName
//                            + " android.permission.WRITE_SECURE_SETTINGS")
//        }
    }

    private fun unlockScreen() {
        val activity = activityRule.activity
        activity.runOnUiThread(
                {
                    activity.window.addFlags((WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                            or WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                            or WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON))
                })
    }

//    private fun disableAnimationsOnTravis() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Settings.Global.putFloat(activityRule.activity.contentResolver,
//                    Settings.Global.ANIMATOR_DURATION_SCALE, 0f)
//        }
//    }

    @Test
    @FlakyTest
    @Throws(Exception::class)
    fun showEditTextAndButtonTheActivityStarts() {
        launchActivity()

        onView(withId(R.id.search_bar)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_search)).check(matches(isDisplayed()))
    }

    @Test
    @FlakyTest
    @Throws(Exception::class)
    fun showReposWhenTheActivityStartsAndHasAQueryStringProvided() {
        launchActivity()
        wait300millis()

        onView(withId(R.id.search_bar)).perform(typeText("Hello world!"))
        onView(withId(R.id.btn_search)).perform(click())

        wait300millis()

        while (activityRule.activity.load_more_bar.visibility == View.VISIBLE) {
            wait300millis()
        }

        assertRepoListVisible()
    }



    @Test
    @Throws(Exception::class)
    fun notCrashWhenLaunchingActivityAndRotatingTheScreenSeveralTimes() {
        launchActivity()
        wait300millis()

        onView(withId(R.id.search_bar)).perform(typeText("Skywell"))
        onView(withId(R.id.btn_search)).perform(click())

        wait300millis()

        while (activityRule.activity.load_more_bar.visibility == View.VISIBLE) {
            wait300millis()
        }

        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        wait300millis()
        assertRepoListVisible()
        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        wait300millis()
        assertRepoListVisible()
        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        wait300millis()
        assertRepoListVisible()
        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        wait300millis()
        assertRepoListVisible()
    }

    private fun assertRepoListVisible() {

        assert(activityRule.activity.repo_list.visibility == View.VISIBLE
                || activityRule.activity.no_results_text.visibility == View.VISIBLE)

        onView(withId(R.id.search_toolbar)).check(matches(isDisplayed()))
    }

    private fun launchActivity() {
        val targetContext = getInstrumentation().getTargetContext()
        val intent = Intent(targetContext, MainActivity::class.java)
        activityRule.launchActivity(intent)

        unlockScreen()
//        disableAnimationsOnTravis()
    }


    private fun wait300millis() {
        try {
            Thread.sleep(300)
        } catch (e: InterruptedException) {
            Log.d(MainActivityTest::class.java.name, e.message)
        }

    }
}