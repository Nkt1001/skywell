package appossum.awesome.skywellapp.db

import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by nikitaemelyanov on 02.05.2018.
 */
@RunWith(AndroidJUnit4::class)
class RepoDaoTest : DbTest() {


    @Test
    fun insertReposAndRead() {
        val actual = TestUtil.createRepos(20, "foo", "boo", "doo")
        db.repoDao().insertRepos(actual)

        val ids = actual.map { it.id }
        db.repoDao().loadById(ids).test().assertValue {
            it.size == actual.size && actual.containsAll(it)
        }
    }

    @Test
    fun insertResponseAndRead() {
        val idList = TestUtil.createRepos(20, "azaz", "hshs", "cccxa").map { it.id }

        val actual = RepoSearchResult("hiiiddi", idList, 20, 3)
        db.repoDao().insert(actual)

        db.repoDao().search(actual.query).test().assertValue {
            actual.repoIds.size == it.repoIds.size && actual.repoIds.containsAll(it.repoIds)
                    && actual.query == it.query && actual.next == it.next
        }
    }

    @Test
    fun insertResponseNull() {
        db.repoDao().search("ksjjc092cn").test().assertError { true }
    }
}