package appossum.awesome.skywellapp.db

object TestUtil {

    fun createRepos(count: Int, owner: String, name: String, description: String): List<Repo> {
        return (0 until count).map {
            createRepo(
                    owner = owner + it,
                    name = name + it,
                    description = description + it
            )
        }
    }

    fun createRepo(owner: String, name: String, description: String) = createRepo(
            id = Repo.UNKNOWN_ID,
            owner = owner,
            name = name,
            description = description
    )

    fun createRepo(id: Int, owner: String, name: String, description: String) = Repo(
            id = id,
            name = name,
            fullName = "$owner/$name",
            description = description,
            owner = Owner(owner, null),
            stars = 3
    )
}