package appossum.awesome.skywellapp

import android.app.Application
import appossum.awesome.skywellapp.db.GithubDb
import appossum.awesome.skywellapp.github.GithubApi
import com.facebook.stetho.Stetho
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by nikitaemelyanov on 30.04.2018.
 */
class App: Application() {

    val ghService: GithubApi = Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(GithubApi::class.java)
    lateinit var db: GithubDb

    override fun onCreate() {
        super.onCreate()

        db = GithubDb.getInstance(this)

        Stetho.initializeWithDefaults(this)
    }
//    val ghDb: GithubDb = Room
//            .databaseBuilder(this, GithubDb::class.java, "github.db")
//            .fallbackToDestructiveMigration()
//            .build()

}