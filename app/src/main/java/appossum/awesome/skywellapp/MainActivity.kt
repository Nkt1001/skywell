package appossum.awesome.skywellapp

import android.content.Context
import android.os.Bundle
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.SearchView
import android.widget.Toast
import appossum.awesome.skywellapp.db.Repo
import appossum.awesome.skywellapp.github.GithubDataRepository
import appossum.awesome.skywellapp.github.GithubPresenter
import appossum.awesome.skywellapp.github.MainView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), MainView {

    private lateinit var githubPresenter: GithubPresenter
    private lateinit var adapter: MyAdapter
    private var repoList: List<Repo>? = emptyList()


    private var request: String? = ""

    companion object {
        private const val ARG_REQUEST_STRING = "appossum.awesome.skywellapp.args.MainActivity.ARG_REQUEST_STRING"
        private const val ARG_REPO_LIST = "appossum.awesome.skywellapp.args.MainActivity.ARG_REPO_LIST"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        request = savedInstanceState?.getString(ARG_REQUEST_STRING, "")
        repoList = savedInstanceState?.getParcelableArray(ARG_REPO_LIST)?.toList()?.map { it as Repo }

        val app = application as App

        githubPresenter = GithubPresenter(GithubDataRepository(app.ghService, app.db, app.db.repoDao()))
        adapter = MyAdapter(object: DiffUtil.ItemCallback<Repo>() {
            override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean {
                return oldItem.owner == newItem.owner
                        && oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean {
                return oldItem.description == newItem.description
                        && oldItem.stars == newItem.stars
            }

        })

        btn_search.setOnClickListener {
            if ((it as Button).text == getString(R.string.find_github_repo)) {
                val currentRequest = search_bar.text.toString()
                if (!currentRequest.isNullOrEmpty()) {
                    githubPresenter.getRepo(currentRequest)
                    search_toolbar.setQuery(currentRequest, false)
                    request = currentRequest
                }
            } else {
                githubPresenter.cancelTask()
            }

            dismissKeyboard(search_bar.windowToken)
        }

        search_toolbar.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!query.isNullOrEmpty()) {
                    if (query != null) {
                        githubPresenter.getRepo(query)
                    }
                    request = query
                    load_more_bar.visibility = View.VISIBLE
                }

                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return true
            }

        })
        repo_list.adapter = adapter
        repo_list.layoutManager = LinearLayoutManager(this)

        if (!request.isNullOrEmpty() || repoList != null) {
            search_toolbar.setQuery(request, false)
            adapter.submitList(repoList)
            didLoadRepo()
        } else {
            supportActionBar?.hide()
        }
    }

    override fun onStart() {
        super.onStart()
        githubPresenter.mainView = this
    }

    override fun onStop() {
        super.onStop()
        githubPresenter.stop()
    }

    override fun willLoadRepo() {
        load_more_bar.visibility = View.VISIBLE
        btn_search.text = getString(R.string.cancel_request)
    }

    override fun didLoadRepo() {
        btn_search.visibility = View.GONE
        search_bar.visibility = View.GONE
        load_more_bar.visibility = View.GONE
        btn_search.setText(R.string.find_github_repo)
        supportActionBar?.show()
    }

    override fun showRepo(githubRepo: List<Repo>) {
        repoList = githubRepo
        no_results_text.visibility = View.GONE
        repo_list.visibility = View.VISIBLE
        adapter.submitList(githubRepo)
    }

    override fun showRepoError() {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
    }

    override fun showEmpty(query: String) {
        repoList = emptyList()
        adapter.submitList(repoList)
        repo_list.visibility = View.GONE
        no_results_text.text = getString(R.string.empty_search_result, query)
        no_results_text.visibility = View.VISIBLE
    }

    private fun dismissKeyboard(windowToken: IBinder?) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString(ARG_REQUEST_STRING, request)
        outState?.putParcelableArray(ARG_REPO_LIST, repoList?.toTypedArray())
        super.onSaveInstanceState(outState)
    }

    override fun taskCancelled() {
        btn_search.setText(R.string.find_github_repo)

        btn_search.visibility = View.VISIBLE
        search_bar.visibility = View.VISIBLE
        supportActionBar?.hide()

        no_results_text.visibility = View.GONE
        repo_list.visibility = View.GONE

        load_more_bar.visibility = View.GONE
    }
}
