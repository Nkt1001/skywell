package appossum.awesome.skywellapp

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import appossum.awesome.skywellapp.db.Repo
import kotlinx.android.synthetic.main.gh_repo_item.view.*

/**
 * Created by nikitaemelyanov on 01.05.2018.
 */
class MyAdapter(itemCallback: DiffUtil.ItemCallback<Repo>): ListAdapter<Repo, MyAdapter.MyViewHolder>(itemCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.gh_repo_item, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(repo: Repo) {
            itemView.name.text = repo.name
            itemView.stars.text = repo.stars.toString()
            itemView.desc.text = repo.description
        }
    }
}