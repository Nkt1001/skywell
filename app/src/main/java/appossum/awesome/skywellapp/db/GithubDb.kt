package appossum.awesome.skywellapp.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(
    entities = [Repo::class, RepoSearchResult::class],
    version = 1, exportSchema = false)
abstract class GithubDb : RoomDatabase() {

    abstract fun repoDao(): RepoDao

    companion object {

        @Volatile private var INSTANCE: GithubDb? = null

        fun getInstance(context: Context): GithubDb =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        GithubDb::class.java, "github.db")
                        .build()
    }
}
