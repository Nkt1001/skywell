package appossum.awesome.skywellapp.github

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {
    @GET("search/repositories")
    fun findRepos(@Query("q") query: String): Observable<GithubRepos>
}