package appossum.awesome.skywellapp.github

import appossum.awesome.skywellapp.db.GithubDb
import appossum.awesome.skywellapp.db.Repo
import appossum.awesome.skywellapp.db.RepoDao
import appossum.awesome.skywellapp.db.RepoSearchResult
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GithubDataRepository(private val githubApi: GithubApi, private val db: GithubDb, private val repoDao: RepoDao) {

    companion object {
        private const val RETRY_COUNT : Long = 3
    }

    fun findRepository(query: String): Observable<GithubRepos> {
        return githubApi
                .findRepos(query)
                .subscribeOn(Schedulers.newThread())
                .retry(RETRY_COUNT)
    }

    fun findRepoDb(query: String): Single<RepoSearchResult> {
        return repoDao.search(query)
                .subscribeOn(Schedulers.io())
    }

    fun getReposFromIds(ids: List<Int>): Single<List<Repo>> {
        return repoDao.loadById(ids)
                .subscribeOn(Schedulers.io())
    }

    fun saveRepos(query: String, item: GithubRepos): Completable {
        return Completable.fromAction {
            val repoIds = item.items.map { it.id }
            val repoSearchResult = RepoSearchResult(
                    query = query,
                    repoIds = repoIds,
                    totalCount = item.total,
                    next = item.nextPage
            )
            db.beginTransaction()
            try {
                repoDao.insertRepos(item.items)
                repoDao.insert(repoSearchResult)
                db.setTransactionSuccessful()
            } finally {
                db.endTransaction()
            }
        }.subscribeOn(Schedulers.newThread())
    }
}