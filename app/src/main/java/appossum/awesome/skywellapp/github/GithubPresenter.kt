package appossum.awesome.skywellapp.github

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class GithubPresenter(private val githubRepository: GithubDataRepository) {

    private val nullView = MainView.NullView()
    var mainView: MainView = nullView
    var isCancelled: Boolean = false

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getRepo(query: String) {
        mainView.willLoadRepo()
        isCancelled = false

        val disposable = githubRepository.findRepoDb(query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (isCancelled) {
                        return@subscribe
                    }

                    if (it != null) {
                        fetchReposFromIds(query, it.repoIds)
                    } else {
                        getRepoFromApi(query)
                    }
                }, {
                    if (isCancelled) {
                        return@subscribe
                    }
                    getRepoFromApi(query)
                })

        compositeDisposable.add(disposable)
    }

    fun cancelTask() {
        isCancelled = true
        mainView.taskCancelled()
    }

    private fun fetchReposFromIds(query: String, ids: List<Int>) {
        val disposable = githubRepository.getReposFromIds(ids)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (isCancelled) {
                        return@subscribe
                    }

                    if (it.isNotEmpty()) {
                        mainView.showRepo(it)
                    } else {
                        mainView.showEmpty(query)
                    }
                    mainView.didLoadRepo()
                }, {
                    if (isCancelled) {
                        return@subscribe
                    }
                    mainView.showRepoError()
                })

        compositeDisposable.add(disposable)
    }

    private fun getRepoFromApi(query: String) {
        val disposable = githubRepository.findRepository(query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ repo ->
                    if (isCancelled) {
                        return@subscribe
                    }

                    if (repo.items.isNotEmpty()) {
                        mainView.showRepo(repo.items)
                        saveResults(query, repo)
                    } else {
                        mainView.showEmpty(query)
                    }

                }, {
                    if (isCancelled) {
                        return@subscribe
                    }
                    mainView.showRepoError()
                }, {
                    if (isCancelled) {
                        return@subscribe
                    }
                    mainView::didLoadRepo
                })
        compositeDisposable.add(disposable)
    }


    private fun saveResults(query: String, item: GithubRepos) {
        val disposable = githubRepository.saveRepos(query, item)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {})

        compositeDisposable.add(disposable)
    }

    fun stop() {
        mainView = MainView.NullView()
        compositeDisposable.clear()
    }
}