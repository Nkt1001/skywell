package appossum.awesome.skywellapp.github

import appossum.awesome.skywellapp.db.Repo
import com.google.gson.annotations.SerializedName

/**
 * Created by nikitaemelyanov on 30.04.2018.
 */
data class GithubRepos(
        @SerializedName("total_count")
        val total: Int = 0,
        @SerializedName("items")
        val items: List<Repo>
) {
    var nextPage: Int? = null
}