package appossum.awesome.skywellapp.github

import appossum.awesome.skywellapp.db.Repo

/**
 * Created by nikitaemelyanov on 30.04.2018.
 */
interface MainView {
    fun willLoadRepo()
    fun didLoadRepo()
    fun showRepo(githubRepo: List<Repo>)
    fun showRepoError()
    fun showEmpty(query: String)
    fun taskCancelled()


    class NullView : MainView {
        override fun taskCancelled() {
        }

        override fun showEmpty(query: String) {
        }

        override fun willLoadRepo() {
        }

        override fun didLoadRepo() {
        }

        override fun showRepo(githubRepo: List<Repo>) {
        }

        override fun showRepoError() {
        }

    }
}