package appossum.awesome.skywellapp.db

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Created by nikitaemelyanov on 02.05.2018.
 */

@RunWith(JUnit4::class)
class RepoTest {
    @Test
    fun testObjectCreating() {
        val obj = Repo(12,"weff", "listOf(124,14,4321)",
                "231", Owner("33f","f23f"), 3352)
        val obj2 = obj.copy()

        assert(obj == obj2)
    }
}