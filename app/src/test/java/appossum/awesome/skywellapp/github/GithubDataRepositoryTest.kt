package appossum.awesome.skywellapp.github

import android.arch.core.executor.testing.InstantTaskExecutorRule
import appossum.awesome.skywellapp.db.GithubDb
import appossum.awesome.skywellapp.db.RepoDao
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.util.concurrent.TimeUnit

/**
 * Created by nikitaemelyanov on 02.05.2018.
 */

@RunWith(JUnit4::class)
class GithubDataRepositoryTest {
    private lateinit var repository: GithubDataRepository
    private val dao = Mockito.mock(RepoDao::class.java)
    private val service = Mockito.mock(GithubApi::class.java)
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        val db = Mockito.mock(GithubDb::class.java)
        `when`(db.repoDao()).thenReturn(dao)
        `when`(db.runInTransaction(ArgumentMatchers.any())).thenCallRealMethod()
        repository = GithubDataRepository(service, db, dao)
    }

    @Test
    fun testFindRepoDb_whenNoRepos() {
        `when`(dao.search("fwef")).thenReturn(Single.error { NullPointerException() })

        repository.findRepoDb("fwef")
                .test()
                .awaitDone(1, TimeUnit.SECONDS)
                .assertError { true }
    }

    @Test
    fun testFindReposFromIds_withNoId() {
        val ids = listOf(1, 12, 42)

        `when`(dao.loadById(ids)).thenReturn(Single.error { NullPointerException() })


        repository.getReposFromIds(ids)
                .test()
                .awaitDone(1, TimeUnit.SECONDS)
                .assertError { true }
    }

    @Test
    fun testFindRepoApi_emptyQuery() {
        `when`(service.findRepos("")).thenReturn(Observable.error(NullPointerException()))

        repository.findRepository("")
                .test()
                .awaitDone(1, TimeUnit.SECONDS)
                .assertError { true }
    }

    @Test
    fun testFindRepoApi_fakeQuery() {
        `when`(service.findRepos("@#f23fo3ifj2endcq2o;;")).thenReturn(Observable.just(GithubRepos(0, emptyList())))

        repository.findRepository("@#f23fo3ifj2endcq2o;;")
                .test()
                .awaitDone(1, TimeUnit.SECONDS)
                .assertValue { it.total == 0 }
    }

    @Test
    fun testFindRepoApi_simpleQuery() {
        `when`(service.findRepos("Hello")).thenReturn(Observable.just(GithubRepos(3, emptyList())))

        repository.findRepository("Hello")
                .test()
                .awaitDone(1, TimeUnit.SECONDS)
                .assertComplete()
    }
}